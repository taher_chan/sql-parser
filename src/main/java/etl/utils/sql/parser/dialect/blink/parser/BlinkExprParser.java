package etl.utils.sql.parser.dialect.blink.parser;

import etl.utils.sql.parser.parser.Lexer;
import etl.utils.sql.parser.parser.SQLExprParser;
import etl.utils.sql.parser.parser.SQLParserFeature;
import etl.utils.sql.parser.util.FnvHash;

import java.util.Arrays;

public class BlinkExprParser extends SQLExprParser {
    private final static String[] AGGREGATE_FUNCTIONS;
    private final static long[] AGGREGATE_FUNCTIONS_CODES;

    static {
        String[] strings = {"AVG", "COUNT", "MAX", "MIN", "STDDEV", "SUM", "ROW_NUMBER",
                "ROWNUMBER"};
        AGGREGATE_FUNCTIONS_CODES = FnvHash.fnv1a_64_lower(strings, true);
        AGGREGATE_FUNCTIONS = new String[AGGREGATE_FUNCTIONS_CODES.length];
        for (String str : strings) {
            long hash = FnvHash.fnv1a_64_lower(str);
            int index = Arrays.binarySearch(AGGREGATE_FUNCTIONS_CODES, hash);
            AGGREGATE_FUNCTIONS[index] = str;
        }
    }

    public BlinkExprParser(String sql) {
        this(new BlinkLexer(sql));
        this.lexer.nextToken();
    }

    public BlinkExprParser(String sql, SQLParserFeature... features) {
        this(new BlinkLexer(sql, features));
        this.lexer.nextToken();
    }

    public BlinkExprParser(Lexer lexer) {
        super(lexer);
        this.aggregateFunctions = AGGREGATE_FUNCTIONS;
        this.aggregateFunctionHashCodes = AGGREGATE_FUNCTIONS_CODES;
    }
}
