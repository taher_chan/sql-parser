/*
 * Copyright 1999-2017 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package etl.utils.sql.parser.dialect.kingbase.visitor;

import etl.utils.sql.parser.DbType;
import etl.utils.sql.parser.ast.SQLName;
import etl.utils.sql.parser.ast.statement.SQLSelectStatement;
import etl.utils.sql.parser.ast.statement.SQLTableSource;
import etl.utils.sql.parser.dialect.kingbase.ast.expr.KingBaseTypeCastExpr;
import etl.utils.sql.parser.dialect.kingbase.ast.stmt.*;
import etl.utils.sql.parser.repository.SchemaRepository;
import etl.utils.sql.parser.stat.TableStat;
import etl.utils.sql.parser.stat.TableStat.Mode;
import etl.utils.sql.parser.util.PGUtils;
import etl.utils.sql.parser.visitor.SchemaStatVisitor;

public class KingBaseSchemaStatVisitor extends SchemaStatVisitor implements KingBaseASTVisitor {
    public KingBaseSchemaStatVisitor() {
        super(DbType.kingbase);
    }

    public KingBaseSchemaStatVisitor(SchemaRepository repository) {
        super(repository);
    }

    @Override
    public DbType getDbType() {
        return DbType.kingbase;
    }

    @Override
    public boolean visit(KingBaseDeleteStatement x) {
        if (repository != null
                && x.getParent() == null) {
            repository.resolve(x);
        }

        if (x.getWith() != null) {
            x.getWith().accept(this);
        }

        SQLTableSource using = x.getUsing();
        if (using != null) {
            using.accept(this);
        }

        x.putAttribute("_original_use_mode", getMode());
        setMode(x, Mode.Delete);

        TableStat stat = getTableStat(x.getTableName());
        stat.incrementDeleteCount();

        accept(x.getWhere());

        return false;
    }

    @Override
    public boolean visit(KingBaseInsertStatement x) {
        if (repository != null
                && x.getParent() == null) {
            repository.resolve(x);
        }

        if (x.getWith() != null) {
            x.getWith().accept(this);
        }

        x.putAttribute("_original_use_mode", getMode());
        setMode(x, Mode.Insert);

        SQLName tableName = x.getTableName();
        {
            TableStat stat = getTableStat(tableName);
            stat.incrementInsertCount();
        }

        accept(x.getColumns());
        accept(x.getQuery());

        return false;
    }

    @Override
    public void endVisit(KingBaseSelectStatement x) {
    }

    @Override
    public boolean visit(KingBaseSelectStatement x) {
        return visit((SQLSelectStatement) x);
    }

    public boolean isPseudoColumn(long hash) {
        return PGUtils.isPseudoColumn(hash);
    }

    @Override
    public boolean visit(KingBaseUpdateStatement x) {
        if (repository != null
                && x.getParent() == null) {
            repository.resolve(x);
        }

        if (x.getWith() != null) {
            x.getWith().accept(this);
        }

        TableStat stat = getTableStat(x.getTableName());
        stat.incrementUpdateCount();

        accept(x.getFrom());

        accept(x.getItems());
        accept(x.getWhere());

        return false;
    }

    @Override
    public boolean visit(KingBaseTypeCastExpr x) {
        x.getExpr().accept(this);
        return false;
    }

    @Override
    public boolean visit(KingBaseShowStatement x) {
        return false;
    }

    @Override
    public boolean visit(KingBaseStartTransactionStatement x) {
        return false;
    }

    @Override
    public boolean visit(KingBaseConnectToStatement x) {
        return false;
    }

    @Override
    public boolean visit(KingBaseCreateSchemaStatement x) {
        return false;
    }

    @Override
    public boolean visit(KingBaseDropSchemaStatement x) {
        return false;
    }

    @Override
    public boolean visit(KingBaseAlterSchemaStatement x) {
        return false;
    }

}
