package etl.utils.sql.parser.dialect.clickhouse.parser;

import etl.utils.sql.parser.ast.SQLExpr;
import etl.utils.sql.parser.ast.SQLOrderBy;
import etl.utils.sql.parser.ast.statement.SQLAssignItem;
import etl.utils.sql.parser.ast.statement.SQLCreateTableStatement;
import etl.utils.sql.parser.parser.SQLCreateTableParser;
import etl.utils.sql.parser.parser.SQLExprParser;
import etl.utils.sql.parser.parser.Token;
import etl.utils.sql.parser.util.FnvHash;
import etl.utils.sql.parser.dialect.clickhouse.ast.ClickhouseCreateTableStatement;

public class ClickhouseCreateTableParser extends SQLCreateTableParser {
    public ClickhouseCreateTableParser(SQLExprParser exprParser) {
        super(exprParser);
    }

    protected SQLCreateTableStatement newCreateStatement() {
        return new ClickhouseCreateTableStatement();
    }

    protected void parseCreateTableRest(SQLCreateTableStatement stmt) {
        ClickhouseCreateTableStatement ckStmt = (ClickhouseCreateTableStatement) stmt;
        if (lexer.identifierEquals(FnvHash.Constants.ENGINE)) {
            lexer.nextToken();
            if (lexer.token() == Token.EQ) {
                lexer.nextToken();
            }
            stmt.setEngine(
                    this.exprParser.expr()
            );
        }

        if (lexer.identifierEquals("PARTITION")) {
            lexer.nextToken();
            accept(Token.BY);
            SQLExpr expr = this.exprParser.expr();
            ckStmt.setPartitionBy(expr);
        }

        if (lexer.token() == Token.ORDER) {
            SQLOrderBy orderBy = this.exprParser.parseOrderBy();
            ckStmt.setOrderBy(orderBy);
        }

        if (lexer.identifierEquals("SAMPLE")) {
            lexer.nextToken();
            accept(Token.BY);
            SQLExpr expr = this.exprParser.expr();
            ckStmt.setSampleBy(expr);
        }

        if (lexer.identifierEquals("SETTINGS")) {
            lexer.nextToken();
            for (; ; ) {
                SQLAssignItem item = this.exprParser.parseAssignItem();
                item.setParent(ckStmt);
                ckStmt.getSettings().add(item);

                if (lexer.token() == Token.COMMA) {
                    lexer.nextToken();
                    continue;
                }

                break;
            }
        }
    }
}
