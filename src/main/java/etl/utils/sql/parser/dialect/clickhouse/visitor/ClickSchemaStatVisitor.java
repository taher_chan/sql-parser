package etl.utils.sql.parser.dialect.clickhouse.visitor;

import etl.utils.sql.parser.DbType;
import etl.utils.sql.parser.repository.SchemaRepository;
import etl.utils.sql.parser.visitor.SchemaStatVisitor;

public class ClickSchemaStatVisitor extends SchemaStatVisitor implements ClickhouseVisitor {
    {
        dbType = DbType.antspark;
    }

    public ClickSchemaStatVisitor() {
        super(DbType.antspark);
    }

    public ClickSchemaStatVisitor(SchemaRepository repository) {
        super(repository);
    }
}
