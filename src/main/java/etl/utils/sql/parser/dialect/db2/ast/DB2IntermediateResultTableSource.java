package etl.utils.sql.parser.dialect.db2.ast;

import etl.utils.sql.parser.ast.statement.SQLTableSourceImpl;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

public class DB2IntermediateResultTableSource extends SQLTableSourceImpl {
    @Override
    protected void accept0(SQLASTVisitor v) {

    }

    public static enum Type {
        OldTable,
        NewTable,
        FinalTable
    }
}
