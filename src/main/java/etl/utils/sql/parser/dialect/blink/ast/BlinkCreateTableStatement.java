package etl.utils.sql.parser.dialect.blink.ast;

import etl.utils.sql.parser.DbType;
import etl.utils.sql.parser.ast.SQLExpr;
import etl.utils.sql.parser.ast.statement.SQLCreateTableStatement;

public class BlinkCreateTableStatement extends SQLCreateTableStatement {
    private SQLExpr periodFor;

    public BlinkCreateTableStatement() {
        dbType = DbType.blink;
    }

    public SQLExpr getPeriodFor() {
        return periodFor;
    }

    public void setPeriodFor(SQLExpr x) {
        if (x != null) {
            x.setParent(this);
        }
        this.periodFor = x;
    }
}
