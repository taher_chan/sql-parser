/*
 * Copyright 1999-2017 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package etl.utils.sql.parser.dialect.odps.ast;

import etl.utils.sql.parser.ast.SQLStatementImpl;
import etl.utils.sql.parser.ast.expr.SQLIdentifierExpr;
import etl.utils.sql.parser.visitor.SQLASTVisitor;
import etl.utils.sql.parser.dialect.odps.visitor.OdpsASTVisitor;

public class OdpsRemoveUserStatement extends SQLStatementImpl {

    private SQLIdentifierExpr user;

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        accept0((OdpsASTVisitor) visitor);
    }

    public void accept0(OdpsASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, user);
        }
        visitor.endVisit(this);
    }

    public SQLIdentifierExpr getUser() {
        return user;
    }

    public void setUser(SQLIdentifierExpr user) {
        if (user != null) {
            user.setParent(this);
        }
        this.user = user;
    }
}
