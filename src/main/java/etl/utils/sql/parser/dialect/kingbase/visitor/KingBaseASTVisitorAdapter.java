/*
 * Copyright 1999-2017 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package etl.utils.sql.parser.dialect.kingbase.visitor;

import etl.utils.sql.parser.dialect.kingbase.ast.expr.*;
import etl.utils.sql.parser.dialect.kingbase.ast.stmt.*;
import etl.utils.sql.parser.visitor.SQLASTVisitorAdapter;

public class KingBaseASTVisitorAdapter extends SQLASTVisitorAdapter implements KingBaseASTVisitor {

    @Override
    public void endVisit(KingBaseSelectQueryBlock.FetchClause x) {

    }

    @Override
    public boolean visit(KingBaseSelectQueryBlock.FetchClause x) {

        return true;
    }

    @Override
    public void endVisit(KingBaseSelectQueryBlock.ForClause x) {

    }

    @Override
    public boolean visit(KingBaseSelectQueryBlock.ForClause x) {

        return true;
    }

    @Override
    public void endVisit(KingBaseDeleteStatement x) {

    }

    @Override
    public boolean visit(KingBaseDeleteStatement x) {

        return true;
    }

    @Override
    public void endVisit(KingBaseInsertStatement x) {

    }

    @Override
    public boolean visit(KingBaseInsertStatement x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseUpdateStatement x) {

    }

    @Override
    public boolean visit(KingBaseUpdateStatement x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseFunctionTableSource x) {

    }

    @Override
    public boolean visit(KingBaseFunctionTableSource x) {
        return true;
    }

    @Override
    public boolean visit(KingBaseTypeCastExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseTypeCastExpr x) {

    }

    @Override
    public void endVisit(KingBaseExtractExpr x) {

    }

    @Override
    public boolean visit(KingBaseExtractExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseBoxExpr x) {

    }

    @Override
    public boolean visit(KingBaseBoxExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBasePointExpr x) {

    }

    @Override
    public boolean visit(KingBasePointExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseMacAddrExpr x) {

    }

    @Override
    public boolean visit(KingBaseMacAddrExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseInetExpr x) {

    }

    @Override
    public boolean visit(KingBaseInetExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseCidrExpr x) {

    }

    @Override
    public boolean visit(KingBaseCidrExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBasePolygonExpr x) {

    }

    @Override
    public boolean visit(KingBasePolygonExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseCircleExpr x) {

    }

    @Override
    public boolean visit(KingBaseCircleExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseLineSegmentsExpr x) {

    }

    @Override
    public boolean visit(KingBaseLineSegmentsExpr x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseShowStatement x) {

    }

    @Override
    public boolean visit(KingBaseShowStatement x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseStartTransactionStatement x) {

    }

    @Override
    public boolean visit(KingBaseStartTransactionStatement x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseConnectToStatement x) {

    }

    @Override
    public boolean visit(KingBaseConnectToStatement x) {
        return true;
    }

    @Override
    public void endVisit(KingBaseCreateSchemaStatement x) {

    }

    @Override
    public boolean visit(KingBaseCreateSchemaStatement x) {
        return false;
    }

    @Override
    public void endVisit(KingBaseDropSchemaStatement x) {

    }

    @Override
    public boolean visit(KingBaseDropSchemaStatement x) {
        return false;
    }

    @Override
    public void endVisit(KingBaseAlterSchemaStatement x) {

    }

    @Override
    public boolean visit(KingBaseAlterSchemaStatement x) {
        return false;
    }

}
