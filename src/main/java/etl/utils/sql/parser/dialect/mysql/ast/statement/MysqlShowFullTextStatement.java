package etl.utils.sql.parser.dialect.mysql.ast.statement;

import etl.utils.sql.parser.dialect.mysql.ast.FullTextType;
import etl.utils.sql.parser.dialect.mysql.visitor.MySqlASTVisitor;

public class MysqlShowFullTextStatement extends MySqlStatementImpl implements MySqlShowStatement {

    private FullTextType type;

    public FullTextType getType() {
        return type;
    }

    public void setType(FullTextType type) {
        this.type = type;
    }

    @Override
    public void accept0(MySqlASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

}
