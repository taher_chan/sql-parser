package etl.utils.sql.parser.dialect.kingbase.ast.expr;

import etl.utils.sql.parser.ast.SQLExpr;
import etl.utils.sql.parser.dialect.kingbase.ast.KingBaseSQLObject;

public interface KingBaseExpr extends SQLExpr, KingBaseSQLObject {

}
