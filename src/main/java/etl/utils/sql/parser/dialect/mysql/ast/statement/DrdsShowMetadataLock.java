package etl.utils.sql.parser.dialect.mysql.ast.statement;

import etl.utils.sql.parser.ast.SQLName;
import etl.utils.sql.parser.ast.statement.SQLShowStatement;
import etl.utils.sql.parser.dialect.mysql.visitor.MySqlASTVisitor;

/**
 * version 1.0
 * Author zzy
 * Date 2019/10/8 20:08
 */
public class DrdsShowMetadataLock extends MySqlStatementImpl implements SQLShowStatement {

    private SQLName schemaName = null;

    public void accept0(MySqlASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

    public SQLName getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(SQLName schemaName) {
        schemaName.setParent(this);
        this.schemaName = schemaName;
    }

}
