package etl.utils.sql.parser.dialect.mysql.ast.statement;

import etl.utils.sql.parser.ast.statement.SQLAlterTableItem;
import etl.utils.sql.parser.dialect.mysql.ast.MySqlObjectImpl;
import etl.utils.sql.parser.dialect.mysql.visitor.MySqlASTVisitor;

/**
 * version 1.0
 * Author zzy
 * Date 2019-06-03 15:43
 */
public class MySqlAlterTableForce extends MySqlObjectImpl implements SQLAlterTableItem {
    @Override
    public void accept0(MySqlASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }
}
