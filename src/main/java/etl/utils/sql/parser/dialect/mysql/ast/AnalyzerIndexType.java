package etl.utils.sql.parser.dialect.mysql.ast;

public enum AnalyzerIndexType {
    INDEX, QUERY
}
