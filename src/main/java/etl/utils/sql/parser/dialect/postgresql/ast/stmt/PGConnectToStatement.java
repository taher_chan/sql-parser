package etl.utils.sql.parser.dialect.postgresql.ast.stmt;

import etl.utils.sql.parser.DbType;
import etl.utils.sql.parser.ast.SQLName;
import etl.utils.sql.parser.ast.SQLStatementImpl;
import etl.utils.sql.parser.dialect.postgresql.visitor.PGASTVisitor;
import etl.utils.sql.parser.visitor.SQLASTVisitor;

public class PGConnectToStatement extends SQLStatementImpl implements PGSQLStatement {
    private SQLName target;

    public PGConnectToStatement() {
        super(DbType.postgresql);
    }

    protected void accept0(SQLASTVisitor visitor) {
        this.accept0((PGASTVisitor) visitor);
    }

    @Override
    public void accept0(PGASTVisitor v) {
        if (v.visit(this)) {
            acceptChild(v, target);
        }
        v.endVisit(this);
    }

    public SQLName getTarget() {
        return target;
    }

    public void setTarget(SQLName x) {
        if (x != null) {
            x.setParent(this);
        }
        this.target = x;
    }
}
