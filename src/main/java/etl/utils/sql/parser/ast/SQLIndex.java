package etl.utils.sql.parser.ast;

import etl.utils.sql.parser.ast.statement.SQLSelectOrderByItem;

import java.util.List;

public interface SQLIndex extends SQLObject {
    List<SQLName> getCovering();

    List<SQLSelectOrderByItem> getColumns();
}
