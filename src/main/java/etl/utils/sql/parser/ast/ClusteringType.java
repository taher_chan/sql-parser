package etl.utils.sql.parser.ast;

public enum ClusteringType {
    Range,
    Hash
}
