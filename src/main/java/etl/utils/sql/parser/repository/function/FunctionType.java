package etl.utils.sql.parser.repository.function;

public enum FunctionType {
    Scalar,
    Aggregate,
    Window
}
