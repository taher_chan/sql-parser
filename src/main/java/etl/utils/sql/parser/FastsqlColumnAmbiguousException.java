package etl.utils.sql.parser;

public class FastsqlColumnAmbiguousException extends FastsqlException {
    public FastsqlColumnAmbiguousException() {

    }

    public FastsqlColumnAmbiguousException(String msg) {
        super(msg);
    }
}
