package etl.utils.sql.parser.semantic;

import etl.utils.sql.parser.FastsqlException;

public class SemanticException extends FastsqlException {
    public SemanticException(String message) {
        super(message);
    }
}
