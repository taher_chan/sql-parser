package etl.utils.sql.parser.semantic;

import etl.utils.sql.parser.DbType;
import etl.utils.sql.parser.SQLUtils;
import etl.utils.sql.parser.ast.SQLStatement;
import etl.utils.sql.parser.ast.statement.SQLCreateTableStatement;
import etl.utils.sql.parser.visitor.SQLASTVisitorAdapter;

import java.util.List;

public class SemanticCheck extends SQLASTVisitorAdapter {

    public static boolean check(String sql, DbType dbType) {
        List<SQLStatement> stmtList = SQLUtils.parseStatements(sql, dbType);

        SemanticCheck v = new SemanticCheck();
        for (SQLStatement stmt : stmtList) {
            stmt.accept(v);
        }

        return false;
    }

    public boolean visit(SQLCreateTableStatement stmt) {
        stmt.containsDuplicateColumnNames(true);
        return true;
    }
}
